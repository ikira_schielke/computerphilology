$.ajaxSetup(
    {
        headers: {
            'X-CSRF-Token': token
        }
    });

var baseUrl = "{{ url('/') }}";

Dropzone.autoDiscover = false;
var myDropzone = new Dropzone("div#dropzoneFileUpload", {
    url: 'drop/upload',
    params: {
        _token: token
    },

    init: function () {
        this.on('success',function (file,response) {
           jQuery.get('drop/refresh', function (data) {
               jQuery('#tablediv').html(data);
           })
        })

    }

});
Dropzone.options.myAwesomeDropzone = {
    paramName: "file", // The name that will be used to transfer the file
    maxFilesize: 2, // MB
    addRemoveLinks: true,
   /* accept: function(file, done) {

    },*/


};



var deleteId = 0;

//ButtonEvent um Element zu löschen, ruft auch Modal auf
jQuery('body').on('click', '.deleteEvent',function () {
    deleteId = jQuery(this).data('id');
    $('#deleteModal').modal('show');


})

//ButtonEvent im Modal, bestätigt löschen
jQuery('body').on('click', '#okModal',function () {
    jQuery.get('drop/delete/'+deleteId, function (data) {
        jQuery('#tablediv').html(data);
    });

    $('#deleteModal').modal('toggle');
})


//ButtonEvent um Elemente zu herunterzuladen
jQuery('body').on('click','.downloadEvent',function(){
    var id = jQuery(this).data('id');
   window.location = 'drop/download/'+id;
})

pagination();

function pagination() {
    $('.pageniationItem').on('click', function () {
        var id = $(this).data('id');
        var choice = $('#choice').val();
        var request = $('#dataTypeInput').val();
        var d_name = $('#dataNameInput').val();
        var school = $('#schoolInput').val();
        var uploader = $('#uploaderInput').val();
        var city = $('#cityInput').val();

        console.log(id+' '+choice);
        $.get('drop/refresh/'+choice+'/'+id+'?request='+request+'&d_name='+d_name+'&school='+school+'&uploader='+uploader+'&city='+city, function (data) {
            $('#tablediv').html(data);
        })
    })
}

$('#choice').on('change', function(){
    var request = $('#dataTypeInput').val();
    var d_name = $('#dataNameInput').val();
    var school = $('#schoolInput').val();
    var uploader = $('#uploaderInput').val();
    var city = $('#cityInput').val();
    var choice = $(this).val();
    var page = 1;


    $.get('drop/search/'+choice+'?request='+request+'&d_name='+d_name+'&school='+school+'&uploader='+uploader+'&city='+city, function(data){
        $('#tablediv').html(data);
    })



    $.get('drop/pagination/'+choice+'/'+page+'?request='+request+'&d_name='+d_name+'&school='+school+'&uploader='+uploader+'&city='+city, function(data){
        $('#pagi').html(data);
        pagination();
    })
});

jQuery('body').on('click', '.dataConfirm', function(){
    var request = $('#dataTypeInput').val();
    var d_name = $('#dataNameInput').val();
    var school = $('#schoolInput').val();
    var uploader = $('#uploaderInput').val();
    var city = $('#cityInput').val();
    var choice = $('#choice').val();
    var page = 1;


    $.get('drop/search/'+choice+'?request='+request+'&d_name='+d_name+'&school='+school+'&uploader='+uploader+'&city='+city, function(data){
        $('#tablediv').html(data);
    })



    $.get('drop/pagination/'+choice+'/'+page+'?request='+request+'&d_name='+d_name+'&school='+school+'&uploader='+uploader+'&city='+city, function(data){
        $('#pagi').html(data);
        pagination();
    })
})

jQuery('body').on('click', '.schoolConfirm', function(){
    var school = $('#schoolInput').val();

    $.get('drop/filter/'+school,function(data){
        $('#tablediv').html(data);
    });

   /* $.get('drop/filter/'+choice+'/'+page+'?school='+school,function(data){
        $('#pagi').html(data);
        pagination();
    });*/
})

jQuery('body').on('click', '.cityConfirm', function(){
    var city = $('#cityInput').val();

    $.get('drop/filter1/'+city,function(data){
        $('#tablediv').html(data);
    });

    /* $.get('drop/filter/'+choice+'/'+page+'?school='+school,function(data){
     $('#pagi').html(data);
     pagination();
     });*/
})

jQuery('body').on('click', '.uploaderConfirm', function(){
    var uploader_name = $('#uploaderInput').val();



    $.get('drop/filter2/'+uploader_name,function(data){
        $('#tablediv').html(data);
    });

    /* $.get('drop/filter/'+choice+'/'+page+'?school='+school,function(data){
     $('#pagi').html(data);
     pagination();
     });*/
})

