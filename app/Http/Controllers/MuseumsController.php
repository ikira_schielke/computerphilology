<?php

namespace App\Http\Controllers;

use App\Upload;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\DB;

use Illuminate\View\View;
use phpDocumentor\Reflection\Types\Array_;

class MuseumsController extends Controller
{
    //
    function getUser($id)
    {
        $user = User::where('id', $id)->first();

        $content = view('user.content', array('user' => $user))->render();

        return view('user.frame', array('content' => $content));
    }

    function index()
    {
        $dbdata = Upload::take(15)->get();

        $pagination = $this->paging(15, 1);
        $content = view('dropzone.content', array('dbdata' => $dbdata, 'pagination' => $pagination))->render();

        return view('dropzone.frame', array('content' => $content));
    }


    public function uploadFiles()
    {

        $input = Input::all();

        $rules = array(
            'file' => 'max:5000000', //angaben byte ggf. vergrößern
        );

        $validation = Validator::make($input, $rules);

        if ($validation->fails()) {
            return Response::make($validation->errors->first(), 400);
        }

        $destinationPath = 'uploads'; // upload path
        $extension = Input::file('file')->getClientOriginalExtension(); // getting file extension
        $fileName = rand(11111, 99999) . '.' . $extension; // renameing image
        $upload_success = Input::file('file')->move($destinationPath, $fileName); // uploading file to given path

        $upload = new Upload();

        $upload->name = Input::file('file')->getClientOriginalName(); /*originalname des bildes wird gehlt und in tabelle abgelegt*/
        $upload->type = Input::file('file')->getClientOriginalExtension();
        $upload->path = $destinationPath . '/' . $fileName;

        $upload->users_id = Auth::user()->id;

        $upload->save();

        if ($upload_success) {
            return $upload;
        } else {
            return Response::json('error', 400);
        }
    }

    function renderTable($choice, $id)
    {

        $d_name = Input::get('d_name');
        $request = Input::get('request');
        $school = Input::get('school');
        $uploader = Input::get('uploader');
        $city = Input::get('city');


        $chunk = new Upload;


        if (isset($d_name) && !empty($d_name)) {
            $chunk = $chunk->where('name', 'LIKE', '%' . $d_name . '%');
        }

        if (isset($request) && !empty($request)) {
            $chunk = $chunk->where('type', 'LIKE', '%' . $request . '%');
        }


        $chunk = $chunk->skip($choice * ($id - 1))->take($choice)->get();


        $table = view('dropzone.table', array('dbdata' => $chunk))->render();

        return $table;
    }

    function search($choice)
    {

        $d_name = Input::get('d_name');
        $request = Input::get('request');
        $school = Input::get('school');
        $uploader = Input::get('uploader');
        $city = Input::get('city');


        $query = new Upload();


        if (isset($d_name) && !empty($d_name)) {
            $query = $query->where('name', 'LIKE', '%' . $d_name . '%');
        }

        if (isset($request) && !empty($request)) {
            $query = $query->where('type', 'LIKE', '%' . $request . '%');
        }

        $query = $query->take($choice)->get();


        return view('dropzone.table', array('dbdata' => $query))->render();
    }

    function deleteElement($id)
    {

        $dbdata = Upload::where('id', $id)->first();
        $tempURL = $dbdata->path;

        if (Auth::user()->id != $dbdata->users_id) {
            return response()->json("Sie sind nicht dazu berechtigt.", 500);
        }
        if (File::exists($tempURL)) { //unlink löscht daten vom server
            unlink($tempURL);
        }

        $result = $dbdata->delete();  //löscht referenz von der datenbank

        if ($result) {
            return $this->renderTable();
        } else {
            return $this->renderTable();
        }

    }

    //Hilfsfunktion von deleteElement()
    function checkDeleted($result)
    {
        if ($result) {
            return response()->json("Datei wurde erfolgreich gelöscht.", 200);
        } else {
            return response()->json("Datei konnte nicht gelöscht werden.", 500);
        }
    }

    //Hilfsfunktion von downloadElement()
    protected function checkDownload($request, $requestName)
    {
        if (is_file($request)) {
            $finfo = finfo_open(FILEINFO_MIME_TYPE);
            $type = finfo_file($finfo, $request);
            finfo_close($finfo);
            //$name = basename($request).PHP_EOL;
            $size = filesize($request);
            header("Content-type: image/jpeg "); //ansonsten : $type
            header("Content-disposition: attachment; filename=$requestName");
            header("Content-size: $size");
            readfile($request); //forces download
            return true;
        } else {
            return false;
        }

    }


    function downloadElement($id)
    {

        $dbdata = Upload::where('id', $id)->first();
        $filename = $dbdata->name;
        $type = $dbdata->type;

        if (!$this->checkDownload($dbdata->path, $filename)) {
            return "Die angeforderte Datei ist leider nicht hinterlegt";

        } else {
            return true;
        }


    }

    public function paging($choice, $page)
    {
        //$choice kann entweder 15,30 oder 50 sein
        //page anzahl der dargestellten (daraus entstehenden) seiten

        $d_name = Input::get('d_name');
        $request = Input::get('request');
        $school = Input::get('school');
        $uploader = Input::get('uploader');
        $city = Input::get('city');


        $all = new Upload;

        if (isset($d_name) && !empty($d_name)) {
            $all = $all->where('name', 'LIKE', '%' . $d_name . '%');
        }

        if (isset($request) && !empty($request)) {
            $all = $all->where('type', 'LIKE', '%' . $request . '%');
        }


        $all = $all->count();


        $value = $all / $choice;

        return view('dropzone.pagination', array('value' => $value, 'page' => $page))->render();


    }

    public function filterSchool($school)
    {

        if (isset($school) && !empty($school)) {
            $user_query = User::where('school', 'LIKE', '%' . $school . '%')->get();

            $user_array = array();

            foreach ($user_query as $user) {
                $upload_query = Upload::where('users_id', $user->id)->get();

                foreach ($upload_query as $upload) {
                    array_push($user_array, $upload);
                }

            }
            return view('dropzone.table', array('dbdata' => $user_array))->render();

        }
        return response()->json("Datei konnte nicht gefunden werden.", 500);


        //$query = $query->join('users','users.id','uploads.users_id')->where('school','LIKE','%'.$school.'%')->select('uploads.name','uploads.type')->get(); //->get()

    }

    public function filterCity($city){


        if (isset($city) && !empty($city)) {
            $user_query = User::where('city', 'LIKE', '%' . $city . '%')->get();

            $user_array = array();

            foreach ($user_query as $user) {
                $upload_query = Upload::where('users_id', $user->id)->get();

                foreach ($upload_query as $upload) {
                    array_push($user_array, $upload);
                }

            }
            return view('dropzone.table', array('dbdata' => $user_array))->render();


        }
        return response()->json("Datei konnte nicht gefunden werden.", 500);
    }

    public function uploaderName($uploader_name){

        if (isset($uploader_name) && !empty($uploader_name)) {
            $user_query = User::where('name', 'LIKE', '%' . $uploader_name . '%')->orwhere('prename','LIKE','%'.$uploader_name.'$')->get();

            $user_array = array();

            foreach ($user_query as $user) {
                $upload_query = Upload::where('users_id', $user->id)->get();

                foreach ($upload_query as $upload) {
                    array_push($user_array, $upload);
                }

            }
            return view('dropzone.table', array('dbdata' => $user_array))->render();

        }
        return response()->json("Datei konnte nicht gefunden werden.", 500);
    }



}
