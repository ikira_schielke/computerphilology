<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Upload extends Model
{
    public $timestamps = false;

    protected $table = 'Uploads';

    protected  $fillable = ['name', 'type', 'path'];

    public function User(){

        return $this->hasOne('App\User');
    }

    public function getUser(){
        return $this->belongsTo('users');
    }
    }
