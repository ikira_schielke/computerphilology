<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>MuseumsApp</title>
    <!-- Own Style Sheets -->
    <link  rel="stylesheet" href="../resources/views/css/all-style.css"  type="text/css">

    <!-- Bootstrap Core CSS -->
    <link href="../startbootstrap-creative-gh-pages/vendor/bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../resources/views/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>

    <!-- Plugin CSS -->
    <link href="../vendor/magnific-popup/magnific-popup.css" rel="stylesheet">

    <!-- Theme CSS -->
    <link href="../resources/views/css/creative.min.css" rel="stylesheet">



    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>


<body>

<header>
    <div class="header-content">
        <div class="flex-center position-ref full-height">
            <!-- muss auskommentiert werden falls php nicht erkannt wird -->
            @if (Route::has('login'))
                <div class="top-right rechts">
                    @if (Auth::check())
                        <a href="{{ url('/drop') }}">Drop</a>
                    @endif
                </div>
            @endif

            <div class="header-content-inner">
                <h1 id="homeHeading">Museums Guides</h1>

                <p>Teile Dein Wissen.</p>
                <a href="{{ url('/login') }}" class="btn btn-primary btn-xl page-scroll">Login</a>
                <!-- build php in here -->
                <a href="{{ url('/register') }}" class="btn btn-primary btn-xl page-scroll">Registrieren</a>
                <div class="dropdown">
                    <a href="#"  class="btn btn-primary dropdown-toggle" role="button" data-toggle="dropdown">Themen
                        <span class="caret"></span></a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Naturgeschichte</a></li>
                        <li><a href="#">Geshichte</a></li>
                        <li><a href="#">Kunst</a></li>
                    </ul>
                </div>


                    <meta name="viewport" content="width=device-width, initial-scale=1">
                    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
                    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
                    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>


            </div>
        </div>
     </div>
</header>


<section class="bg-primary" id="about">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2 text-center">
                <h2 class="section-heading">Teile Dein Wissen mit anderen.</h2>
                <hr class="light">
                <p class="text-faded">Lade Dein Content hoch und teile Deine Museums Guides mit anderen. Nachdem Du dich registriert hast, kannst Du deinen Museums Guide als .odf, .pdf, .docs, .doc und als .jpg hochladen and teilen.</p>
                <a href="{{ url('/register') }}" class="page-scroll btn btn-default btn-xl sr-button">Registriere Dich!</a>
            </div>
        </div>
    </div>
</section>


<!-- jQuery -->
<script src="vendor/jquery/jquery.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="vendor/bootstrap/js/bootstrap.min.js"></script>

<!-- Plugin JavaScript -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
<script src="vendor/scrollreveal/scrollreveal.min.js"></script>
<script src="vendor/magnific-popup/jquery.magnific-popup.min.js"></script>

<!-- Theme JavaScript -->
<script src="js/creative.min.js"></script>
<script src="js/creative.min.js"></script>

</body>
</html>
