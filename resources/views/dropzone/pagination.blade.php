<!DOCTYPE html>
<html lang="en">
<div class="container-fluid">
    <div class="row">
        <div class="col-md-3 ">
            <h3>Einträge pro Seite</h3>
            <select class="form-control" id="choice">
                <option id="15" value="15">15</option>
                <option id="30">30</option>
                <option id="50">50</option>
            </select>
        </div>

        <div class="col-md-3">
            <ul class="pagination">
                <?php
                for($i = 0; $i< $value; $i++){
                    echo '<li><a class="pageniationItem" id="pageNumber'.($i+1).'" data-id="'.($i+1).'">'.($i+1).'</a></li>';
                }
                ?>
            </ul>
        </div>
    </div>
</div>
</html>