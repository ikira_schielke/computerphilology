<table class="table table-striped">
    <thead>
    <tr>
        <th id="firstHeading">Dateiname</th>
        <th id="secondHeading">Dateiformat</th>
        <th id="thirdHeading">Bearbeiten</th>
        <th id="fourthHeading">Herunterladen</th>
    </tr>
    </thead>
    <tbody>
    @foreach($dbdata as $data)
        <tr>
            <td>{{$data->name}}</td>
            <td>{{$data->type}}</td>
            <td><a class="btn btn-danger btn-xs deleteEvent" data-id="{{$data->id}}"><i class="fa fa-trash-o"></i></a></td>
            <td><a class="btn btn-success btn-xs downloadEvent" data-id="{{$data->id}}"><i class="fa fa-download"></i></a></td>
        </tr>
    @endforeach
    </tbody>
</table>




