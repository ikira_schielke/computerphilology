<div class="container-fluid">

    <div class="row">

        <div class="col-md-6">

            <h2>Filter nach:</h2>
            <div class="row" id="sub-row-1">


                <div class="form-group">

                    <div class="well">
                        <label for="dataTypeInput">Dateiformat</label>
                        <input class="form-control"  id="dataTypeInput" type="text">

                        <label for="dataNameInput">Dateiname</label>
                        <input class="form-control" id="dataNameInput" type="text">
                        <a class="btn btn-danger btn-s dataConfirm">Suchen</a>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-body">
                            <label for="schoolInput">Schule</label>
                            <input class="form-control" id="schoolInput" type="text">
                            <a class="btn btn-success btn-s schoolConfirm">Suchen</a>
                        </div>
                    </div>

                    <div class="well">
                        <label for="uploader">UploaderIn</label>
                        <input class="form-control" id="uploaderInput" type="text">
                        <a class="btn btn-info btn-s uploaderConfirm">Suchen</a>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-body">
                            <label for="cityInput">Stadt</label>
                            <input class="form-control" id="cityInput" type="text" data-toggle="tooltip"
                                   title="Ortsname der Schule, die der User besucht.">
                            <a class="btn btn-warning btn-s cityConfirm">Suchen</a>
                        </div>
                    </div>

                </div>

            </div>

            <div class="row" id="sub-row-4">
                <div class="dropzone" id="dropzoneFileUpload">
                </div>
            </div>

        </div>

        <div id="tablediv" class=" col-md-6">
            <table id="pagination" class="table table-striped">
                <thead>
                <tr>
                    <th id="firstHeading">Dateiname</th>
                    <th id="secondHeading">Dateiformat</th>
                    <th id="thirdHeading">Bearbeiten</th>
                    <th id="fourthHeading">Herunterladen</th>
                </tr>
                </thead>
                <tbody>
                @foreach($dbdata as $data)
                    <tr>
                        <td>{{$data->name}}</td>
                        <td>{{$data->type}}</td>
                        <td><a class="btn btn-danger btn-xs deleteEvent" data-id="{{$data->id}}"><i
                                        class="fa fa-trash-o"></i></a></td>
                        <td><a class="btn btn-success btn-xs downloadEvent" data-id="{{$data->id}}"><i
                                        class="fa fa-download"></i></a></td> <!--href="{{$data->path}}" download-->
                    </tr>
                @endforeach
                </tbody>
            </table>

        </div>

    </div>

    <div class="row">
        <div class="-align-center col-md-6" id="pagi">
            <!--hier kommt das blade für pagination rein-->
            {!! $pagination !!}
        </div>

    </div>


    <!--script for hover event/tooltip -->
    <script>
        $(document).ready(function () {
            $('[data-toggle="tooltip"]').tooltip();
        });
    </script>

    <!-- jQuery Version 1.11.1 -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>


</div>



