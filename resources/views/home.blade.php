@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">

                <div class="panel-heading">Hoppla!</div>

                <div class="panel-body">
                    
                    Hast du dich verlaufen?
                    Hier geht es weiter zum Down- und Upload Bereich
                    <a href="/museumsapp/public/drop" type="button" class="btn btn-success btn-lg btn-block"> <i class="fa fa-clipboard" aria-hidden="true"></i><i class="fa fa-arrow-right" aria-hidden="true"></i></a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
