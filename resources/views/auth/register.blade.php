@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Registrierung</div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/register') }}">
                        {{ csrf_field() }}

                        <!-- ZUERST VORNAME -->
                        <div class="form-group{{ $errors->has('prename') ? ' has-error' : '' }}"> <!--ZU PRENAME ÄNDERN-->
                            <label for="prename" class="col-md-4 control-label">Vorname</label>

                            <div class="col-md-6">
                                <input id="prename" type="text" class="form-control" name="prename" value="{{ old('prename') }}" required autofocus>

                                @if ($errors->has('prename'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('prename') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <!--Neue Metadaten für Registrierung -->
                        <!--NACHNAME-->
                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Nachname</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                         <!--Schule-->
                            <div class="form-group{{ $errors->has('school') ? ' has-error' : '' }}">
                                <label for="school" class="col-md-4 control-label">Schule</label>

                                <div class="col-md-6">
                                    <input id="school" type="text" class="form-control" name="school" value="{{ old('school') }}" required autofocus>

                                    @if ($errors->has('school'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('school') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <!--Stadt-->
                            <div class="form-group{{ $errors->has('city') ? ' has-error' : '' }}">
                                <label for="city" class="col-md-4 control-label">Stadt</label>

                                <div class="col-md-6">
                                    <input id="city" type="text" class="form-control" name="city" value="{{ old('city') }}" required autofocus>

                                    @if ($errors->has('city'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('city') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>




                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail Addresse</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Passwort</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="password-confirm" class="col-md-4 control-label">Passwort Bestätigen</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Registrieren
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
