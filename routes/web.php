<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*HIER WERDEN DIE URL PFADE ANGELEGT*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

/* über die group und MIDDLEWARE kann nicht mehr über eine korrekte url OHNE authentifizierung zugegriffen werden*/
Route::group(['middleware'=>'auth'],function (){

    Route::get('/home', 'HomeController@index');

    Route::get('/user/{id}', 'MuseumsController@getUser'); /*/user/ ist oben url-parameter der zweite paramter sind interne routes methode getUser wird aufgerufen*/

    Route::get('/drop', 'MuseumsController@index');

    Route::post('/drop/upload', 'MuseumsController@uploadFiles');

    Route::get('/drop/refresh/{choice}/{id}', 'MuseumsController@renderTable'); //checken wie und ob parameter übergeben werden müssen

    Route::get('/drop/delete/{id}', 'MuseumsController@deleteElement');

    Route::get('/drop/download/{id}', 'MuseumsController@downloadElement');

    Route::any('/drop/search/{choice}','MuseumsController@search');

    Route::get('/drop/pagination/{choice}/{page}','MuseumsController@paging');

    Route::get('/drop/filter/{school}','MuseumsController@filterSchool'); //filter nur für schule!

    Route::get('/drop/filter1/{city}', 'MuseumsController@filterCity'); //filter nur für die Stadt

    Route::get('/drop/filter2/{uploader_name}', 'MuseumsController@uploaderName'); // filter nur für Uploadernamen
});

